from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListEditForm


# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/list.html", context)


def show_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "detail": detail,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_list(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListEditForm(request.POST, instance=detail)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.id)
    else:
        form = TodoListEditForm(instance=detail)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)
