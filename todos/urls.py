from django.urls import path
from todos.views import todo_list_list, show_detail, create_list

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_detail, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
]
